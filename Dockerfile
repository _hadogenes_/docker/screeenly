FROM php:8-apache

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

USER root

ENV SCREEENLY_DISABLE_SANDBOX=true \
    FILESYSTEM_DRIVER=public

ADD entrypoint.sh /
ADD 006-screeenly.conf /etc/apache2/sites-available/

RUN set -x \
 && export DEBIAN_FRONTEND="noninteractive" \
 && apt update \
 && apt install -y \
      curl \
      nodejs gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget \
 && apt install -y \
      libc-dev \
      libpq5 libzip4 \
      yarnpkg npm \
      git zip unzip sudo \
      chromium \
 \
 && install-php-extensions iconv exif gd pdo pdo_mysql mysqli zip \
 && npm install --global envsub \
 && npm install --global --unsafe-perm puppeteer \
 \
 && a2enmod rewrite \
 && a2dissite 000-default.conf \
 && a2ensite 006-screeenly.conf \
 \
 && apt purge -y libc-dev \
 && apt autoremove --purge -y \
 && apt clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
 \
 && chown www-data:www-data /var/www/ -R

USER www-data

RUN set -x \
 && cd /var/www \
 && wget "https://github.com/stefanzweifel/screeenly/archive/master.zip" -O master.zip \
 && unzip master.zip \
 && rm master.zip \
 && mv screeenly-master screeenly \
 \
 && cd screeenly \
 && sed -i '15,19s/timeout([0-9][0-9]*)/timeout(120)/' modules/Screeenly/Services/ChromeBrowser.php \
 && composer update \
 && composer install \
 && yarnpkg install \
 && npm install \
 && npm run prod \
 \
 && rm -rf \
     .gitattributes .gitignore .travis.yml CODE_OF_CONDUCT.md \
     /tmp/* /var/tmp/* /var/www/.cache /var/www/.yarn /var/www/.npm /var/www/.composer /var/www/.wget-hsts

WORKDIR /var/www/screeenly
USER root

# initial setup
CMD /entrypoint.sh
