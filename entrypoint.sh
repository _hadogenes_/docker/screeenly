#!/bin/sh

if [ "$PUID" ]; then
    WWW_DATA_UID=$(id -u www-data)
    if [ "$PUID" != "$WWW_DATA_UID" ]; then
        usermod --uid $PUID www-data
        chown -R www-data /var/www/
    fi
fi

if [ "$PGID" ]; then
    WWW_DATA_GID=$(id -g www-data)
    if [ "$PGID" != "$WWW_DATA_GID" ]; then
        groupmod --gid $PGID --non-unique www-data
        chown -R :www-data /var/www/
    fi
fi


#sudo -E -u www-data envsub env.template .env
sudo -E -u www-data php artisan migrate --force

exec apache2-foreground
